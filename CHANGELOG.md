# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## 1.0.0-beta3 - 2024-04-11
### Added
- Set and retrieve proxied server to which root location of a domain will be
  mapped (Closes #3)

## 1.0.0-beta2 - 2021-07-14
### Changed
- Remove `root` and `index` directives from the default template

## 1.0.0-beta1 - 2021-01-19
### Added
- Base class to define different kind of domains in `webdomains.core`
- Classes to work with NGINX domains and certificate generation with dehydrated
- `webdomains` command-line interface to manage NGINX domains with HTTPS support
